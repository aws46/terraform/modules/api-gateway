output "http_resource_id" {
  value = aws_api_gateway_resource.http_resource.id
}

output "http_resource_path" {
  value = aws_api_gateway_resource.http_resource.path
}

output "http_resource_path_part" {
  value = aws_api_gateway_resource.http_resource.path_part
}

output "aws_api_gateway_deployment_url" {
  value = aws_api_gateway_deployment.gateway_deployment.invoke_url
}