# Introduction
This module creates a Gateway API. It can attach the endpoint either to a `lambda` or `load balancer` via `vpc_link`
