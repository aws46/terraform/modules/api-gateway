resource "aws_api_gateway_authorizer" "authorizer" {
  count = local.provide_authorizer ? 1 : 0

  name                   = "enforcement_layer_auth"
  rest_api_id            = var.rest_api_id
  authorizer_uri         = join(",", data.aws_lambda_function.authorizer_lambda_function.*.invoke_arn)
  authorizer_credentials = join("", aws_iam_role.invocation_role.*.arn)
  type                   = "REQUEST"

  authorizer_result_ttl_in_seconds = 0
  identity_source                  = var.identity_source //join(",", ["method.request.header.Authorization", "context.httpMethod", "context.path"])
}

data "aws_lambda_function" "authorizer_lambda_function" {
  count = local.provide_authorizer ? 1 : 0

  function_name = var.authorizer_lambda_function_name
}

resource "aws_iam_role" "invocation_role" {
  count = local.provide_authorizer ? 1 : 0

  name = join("-", [var.pipeline_environment, local.resource_names.api-gateway, local.resource_names.authorization])
  // path = "/"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "apigateway.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF

  tags = var.tags
}

resource "aws_iam_role_policy" "invocation_policy" {
  count = local.provide_authorizer ? 1 : 0

  name = "apigatewayLambdaAuthorization"
  role = join("", aws_iam_role.invocation_role.*.id)

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "lambda:InvokeFunction",
      "Effect": "Allow",
      "Resource": "${data.aws_lambda_function.authorizer_lambda_function[0].arn}"
    }
  ]
}
EOF
}