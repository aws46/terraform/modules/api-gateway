resource "aws_api_gateway_rest_api" "example_api" {
  name = "example"
}

module "example_module" {
  source = "../"

  pipeline_environment = var.pipeline_environment


  region       = var.aws_region
  allow_origin = null

  is_with_vpc_link_connected = false

  path_part = "person/{firstname}"

  request_path_parameters  = ["firstname"]
  request_query_parameters = ["nameStartsWith"]
  # authorizer_lambda_function_name = var.enforcement_lambda_function_name #disabled for now.

  lambda_function_name           = ""
  lambda_arn                     = ""
  lambda_invoke_arn              = ""
  is_resource_attached_to_lambda = true

  rest_api_id          = aws_api_gateway_rest_api.example_api.id
  parent_resource_id   = aws_api_gateway_rest_api.example_api.root_resource_id
  resource_http_method = "GET"

  integration_input_type = "AWS"

  tags = var.tags
}
