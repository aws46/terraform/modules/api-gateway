variable "pipeline_environment" {
  type = string
}

variable "aws_region" {
  type = string
}

variable "tags" {
  type = map(string)
}