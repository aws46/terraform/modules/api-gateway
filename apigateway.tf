// Path /authenticator
resource "aws_api_gateway_resource" "http_resource" {
  rest_api_id = var.rest_api_id
  parent_id   = var.parent_resource_id
  path_part   = var.path_part
}

resource "aws_api_gateway_method" "resource_method" {
  depends_on = [
    aws_api_gateway_resource.http_resource,
  ]

  rest_api_id   = var.rest_api_id
  resource_id   = aws_api_gateway_resource.http_resource.id
  http_method   = var.resource_http_method
  authorization = local.provide_authorizer ? "CUSTOM" : "NONE"
  authorizer_id = local.provide_authorizer ? join("", aws_api_gateway_authorizer.authorizer.*.id) : null

  /* request_parameters = var.is_vpc_link_create ? {
    "method.request.path.name" = true
  } : null*/
  request_parameters = var.is_with_vpc_link_connected && ! local.attach_resource_to_lambda ? merge(local.method_request_path_parameters, local.method_request_query_parameters, local.method_request_header_parameters) : null
}

resource "aws_api_gateway_integration" "method_integration" {
  depends_on = [
    aws_api_gateway_method.resource_method
  ]

  rest_api_id             = var.rest_api_id
  resource_id             = aws_api_gateway_resource.http_resource.id
  http_method             = aws_api_gateway_method.resource_method.http_method
  integration_http_method = local.attach_resource_to_lambda ? "POST" : var.resource_http_method // Lambda always uses POST
  type                    = var.integration_input_type
  uri                     = local.attach_resource_to_lambda ? (var.lambda_invoke_arn == null ? "invalid" : var.lambda_invoke_arn) : local.resource_endpoint_vpc_link_url_part
  request_templates = {
    "application/json" = "$input.json('$')"
  }

  connection_type = var.is_with_vpc_link_connected && ! local.attach_resource_to_lambda ? "VPC_LINK" : null
  connection_id   = var.is_with_vpc_link_connected && ! local.attach_resource_to_lambda ? var.vpc_link_id : null

  request_parameters = var.is_with_vpc_link_connected && ! local.attach_resource_to_lambda ? merge(local.integration_request_path_parameters, local.integration_request_query_parameters, local.integration_request_header_parameters) : null

  /* request_parameters = var.is_with_vpc_link_connected && !var.is_resource_attached_to_lambda ? {
    "integration.request.path.name" = "method.request.path.name"
  } : null*/
}

resource "aws_api_gateway_method_response" "method_response_200" {
  depends_on = [
    aws_api_gateway_resource.http_resource,
    aws_api_gateway_method.resource_method,
    aws_api_gateway_integration.method_integration
  ]

  rest_api_id = var.rest_api_id
  resource_id = aws_api_gateway_resource.http_resource.id
  http_method = aws_api_gateway_method.resource_method.http_method
  status_code = "200"

  response_models     = var.method_response_models
  response_parameters = var.is_cors_enabled ? local.method_response_parameters : null
}

resource "aws_api_gateway_integration_response" "integration_response_200" {
  depends_on = [
    aws_api_gateway_resource.http_resource,
    aws_api_gateway_method.resource_method,
    aws_api_gateway_integration.method_integration,
    aws_api_gateway_method_response.method_response_200
  ]

  rest_api_id = var.rest_api_id
  resource_id = aws_api_gateway_resource.http_resource.id
  http_method = aws_api_gateway_method.resource_method.http_method
  status_code = aws_api_gateway_method_response.method_response_200.status_code

  response_parameters = var.is_cors_enabled ? local.integration_response_parameters : null

}

resource "aws_api_gateway_method_response" "method_response_401" {
  depends_on = [
    aws_api_gateway_resource.http_resource,
    aws_api_gateway_method.resource_method,
    aws_api_gateway_integration.method_integration
  ]

  rest_api_id = var.rest_api_id
  resource_id = aws_api_gateway_resource.http_resource.id
  http_method = aws_api_gateway_method.resource_method.http_method
  status_code = "401"

  response_parameters = var.is_cors_enabled ? local.method_response_parameters : null

  response_models = var.method_response_models
}

resource "aws_api_gateway_integration_response" "integration_response_401" {
  depends_on = [
    aws_api_gateway_resource.http_resource,
    aws_api_gateway_method.resource_method,
    aws_api_gateway_integration.method_integration,
    aws_api_gateway_method_response.method_response_401
  ]

  rest_api_id = var.rest_api_id
  resource_id = aws_api_gateway_resource.http_resource.id
  http_method = aws_api_gateway_method.resource_method.http_method
  status_code = aws_api_gateway_method_response.method_response_401.status_code

  selection_pattern = ".*\\\"code\\\":\\s?401.*"

  response_parameters = var.is_cors_enabled ? local.integration_response_parameters : null

  response_templates = {
    "application/json" = <<EOT
#set ($errorMessageObj = $util.parseJson($input.path('$.errorMessage')))
{
  "code" : "$errorMessageObj.code",
  "err" : "$errorMessageObj.message"
}
EOT
  }
}

resource "aws_api_gateway_method_response" "method_response_500" {
  depends_on = [
    aws_api_gateway_resource.http_resource,
    aws_api_gateway_method.resource_method,
    aws_api_gateway_integration.method_integration
  ]

  rest_api_id = var.rest_api_id
  resource_id = aws_api_gateway_resource.http_resource.id
  http_method = aws_api_gateway_method.resource_method.http_method
  status_code = "500"


  response_parameters = var.is_cors_enabled ? local.method_response_parameters : null
  response_models     = var.method_response_models
}

resource "aws_api_gateway_integration_response" "integration_response_500" {
  depends_on = [
    /* aws_api_gateway_resource.http_resource,
    aws_api_gateway_method.resource_method,*/
    aws_api_gateway_integration.method_integration,
    aws_api_gateway_method_response.method_response_500
  ]

  rest_api_id = var.rest_api_id
  resource_id = aws_api_gateway_resource.http_resource.id
  http_method = aws_api_gateway_method.resource_method.http_method
  status_code = aws_api_gateway_method_response.method_response_500.status_code

  selection_pattern   = ".*\\\"code\\\":\\s?500.*"
  response_parameters = var.is_cors_enabled ? local.integration_response_parameters : null

  response_templates = {
    "application/json" = <<EOT
#set ($errorMessageObj = $util.parseJson($input.path('$.errorMessage')))
{
  "code" : "$errorMessageObj.code",
  "err" : "$errorMessageObj.message"
}
EOT
  }
}

module "cors" {
  //source = "git::https://github.com/squidfunk/terraform-aws-api-gateway-enable-cors.git?ref=tags/0.3.1"
  source = "./modules/cors"

  is_enabled        = var.is_cors_enabled
  allow_origin      = local.allow_origin
  allow_credentials = var.allow_credentials
  api_id            = var.rest_api_id
  api_resource_id   = aws_api_gateway_resource.http_resource.id
}

//DEPLOYMENT
resource "aws_api_gateway_deployment" "gateway_deployment" {
  depends_on = [
    aws_api_gateway_resource.http_resource,
    aws_api_gateway_method.resource_method,
    aws_api_gateway_integration.method_integration,
    aws_api_gateway_integration_response.integration_response_200,
    aws_api_gateway_integration_response.integration_response_401,
    aws_api_gateway_integration_response.integration_response_500,
    module.cors
  ]

  rest_api_id = var.rest_api_id
  stage_name  = var.pipeline_environment

  variables = {
    deployed_at = timestamp()
  }
}