#LAMBDA
data "aws_arn" "lambda_arn" {
  count = var.is_resource_attached_to_lambda ? 1 : 0

  arn = var.lambda_arn
}

# Grant execute permissions
# More: http://docs.aws.amazon.com/apigateway/latest/developerguide/api-gateway-control-access-using-iam-policies-to-invoke-api.html
resource "aws_lambda_permission" "lambda_invoke_permissions" {
  count = var.is_resource_attached_to_lambda ? 1 : 0

  statement_id  = "${var.prefix}AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = var.lambda_function_name
  principal     = "apigateway.amazonaws.com"
  source_arn    = "arn:aws:execute-api:${data.aws_arn.lambda_arn[0].region}:${data.aws_arn.lambda_arn[0].account}:${var.rest_api_id}/*/${aws_api_gateway_method.resource_method.http_method}${aws_api_gateway_resource.http_resource.path}"
}