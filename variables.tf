variable "pipeline_environment" {
  type = string
}

variable "region" {
  type = string
}

variable "prefix" {
  type = string
}

variable "rest_api_id" {
  type = string
}

variable "parent_resource_id" {
  type = string
}

variable "resource_http_method" {
  type = string
}

variable "path_part" {
  type = string
  //default = null
}

variable "tags" {
  type = map(string)
}

variable "allow_credentials" {
  description = "Allow credentials"
  default     = true
}

variable "allow_origin" {
  type    = string
  default = null
}

#AUTHORIZER
variable "authorizer_lambda_function_name" {
  type    = string
  default = null
}

variable "identity_source" {
  type    = string
  default = "method.request.header.Authorization"
}


#LAMBDA_VARIABLES
variable "lambda_arn" {
  type    = string
  default = null
}

variable "lambda_invoke_arn" {
  type    = string
  default = null
}

variable "lambda_function_name" {
  type    = string
  default = null
}

//API GATEWAY VARIABLES

variable "integration_input_type" {
  type    = string
  default = "AWS"
}

variable "request_path_parameters" {
  type    = list(string)
  default = []
}

variable "request_query_parameters" {
  type    = list(string)
  default = []
}

variable "request_headers" {
  type    = list(string)
  default = []
}
variable "method_response_models" {
  type = map
  default = {
    "application/json" = "Empty"
  }
}

#VPC_LINK_RELATED
variable "vpc_link_id" {
  type    = string
  default = null
}

variable "resource_endpoint_vpc_link_url_part" {
  type    = string
  default = null
}

variable "nlb_dns_name" {
  type    = string
  default = null
}


/*
======= BOOLEAN VARIABLES
*/

variable "is_resource_attached_to_lambda" {
  type = bool
}

variable "is_cors_enabled" {
  type    = bool
  default = true
}

variable "is_with_vpc_link_connected" {
  type = bool
}

/*variable "is_method_under_root_resource" {
  type = bool
  default = true
}*/

locals {

  resource_names = {
    vpc_link      = join("-", [var.pipeline_environment, "vpc-link"]) //change this naming
    authorization = "auth"
    api-gateway   = "api-gateway"
  }

  resource_endpoint_vpc_link_url_part = var.is_with_vpc_link_connected ? (var.resource_endpoint_vpc_link_url_part == null ? "http://${var.nlb_dns_name}" : "http://${var.nlb_dns_name}/${var.resource_endpoint_vpc_link_url_part}") : null

  //CORS
  allow_origin = var.allow_origin == null ? "" : var.allow_origin

  headers = map(
    "Access-Control-Allow-Headers", "'${join(",", var.allow_headers)}'",
    "Access-Control-Allow-Methods", "'${join(",", var.allow_methods)}'",
    "Access-Control-Allow-Origin", "'${local.allow_origin}'",
    "Access-Control-Max-Age", "'${var.allow_max_age}'",
    "Access-Control-Allow-Credentials", var.allow_credentials ? "'true'" : ""
  )

  header_values = compact(values(local.headers))

  //Integration response parameters
  parameter_names = formatlist("method.response.header.%s", local.header_names)

  # Pick names that from non-empty header values
  header_names = matchkeys(
    keys(local.headers),
    values(local.headers),
    local.header_values
  )

  integration_response_parameters = zipmap(
    local.parameter_names,
    local.header_values
  )

  true_list = split("|",
    replace(join("|", local.parameter_names), "/[^|]+/", "true")
  )

  method_response_parameters = zipmap(
    local.parameter_names,
    local.true_list
  )

  //method request path parameters setup
  method_request_parameter_names          = length(var.request_path_parameters) > 0 ? formatlist("method.request.path.%s", var.request_path_parameters) : []
  method_request_path_parameter_true_list = length(var.request_path_parameters) > 0 ? split("|", replace(join("|", local.method_request_parameter_names), "/[^|]+/", "true")) : []
  method_request_path_parameters          = length(var.request_path_parameters) > 0 ? zipmap(local.method_request_parameter_names, local.method_request_path_parameter_true_list) : {}

  integration_request_path_names      = formatlist("integration.request.path.%s", var.request_path_parameters)
  integration_request_path_parameters = zipmap(local.integration_request_path_names, local.method_request_parameter_names)

  //method request query parameters setup
  method_request_query_names      = length(var.request_query_parameters) > 0 ? formatlist("method.request.querystring.%s", var.request_query_parameters) : []
  method_request_query_true_list  = length(var.request_query_parameters) > 0 ? split("|", replace(join("|", local.method_request_query_names), "/[^|]+/", "true")) : []
  method_request_query_parameters = length(var.request_query_parameters) > 0 ? zipmap(local.method_request_query_names, local.method_request_query_true_list) : {}

  integration_request_query_names      = formatlist("integration.request.querystring.%s", var.request_query_parameters)
  integration_request_query_parameters = zipmap(local.integration_request_query_names, local.method_request_query_names)

  //request headers
  method_request_header_names      = length(var.request_headers) > 0 ? formatlist("method.request.header.%s", var.request_headers) : []
  method_request_header_true_list  = length(var.request_headers) > 0 ? split("|", replace(join("|", local.method_request_header_names), "/[^|]+/", "true")) : []
  method_request_header_parameters = length(var.request_headers) > 0 ? zipmap(local.method_request_header_names, local.method_request_header_true_list) : {}

  integration_request_header_name       = formatlist("integration.request.header.%s", var.request_headers)
  integration_request_header_parameters = zipmap(local.integration_request_header_name, local.method_request_header_names)

  //boolean parameters
  attach_resource_to_lambda = (var.lambda_function_name != null && var.lambda_arn != null && var.lambda_invoke_arn != null && var.is_resource_attached_to_lambda) ? true : false
  provide_authorizer        = var.authorizer_lambda_function_name == null ? false : true
}